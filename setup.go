package helloworld

import (
	"github.com/mholt/caddy"
	"github.com/mholt/caddy/caddyhttp/httpserver"
)

func init() {
	caddy.RegisterPlugin("helloworld", caddy.Plugin{
		ServerType: "http",
		Action:     setupHelloWorld,
	})

	// add a non public directive until we are ready to release
	httpserver.RegisterDevDirective("helloworld", "jwt")
}

func setupHelloWorld(c *caddy.Controller) error {
	cfg := httpserver.GetConfig(c)
	hws := HelloWorldStore{}

	for c.Next() { // skip the directive name
		if !c.NextArg() {
			return c.ArgErr()
		}
		name :=  c.Val()
		hws.Names = append(hws.Names, name)
	}

	// wire-up handler to a middleware
	mid := func(next httpserver.Handler) httpserver.Handler {
		hws.Next = next
		return hws
	}

	cfg.AddMiddleware(mid)

	return nil
}