# caddy-helloworld

An example Caddy plugin with the minimal boillerplate to implement a HTTP Middleware and
custom directive configuration.

This plugin is not part of official release, so to test it with caddy, you need to patch caddy and
add it to the import list (see: `caddy/caddymain/run.go` in caddy source-code) .

I'm registering the configuration directive using the `httpserver.RegisterDevDirective` function, 
in an official plugin you probably want to open a PR and implement the directive registration in
caddy source-code.

To test the plugin, you need to create  a Caddyfile similar to the example below:

```
localhost:8080 {
  root /path/to/wwwroot
  helloworld Name
  helloworld AnotherName
}
```
