package helloworld

import (
	"net/http"
	"github.com/mholt/caddy/caddyhttp/httpserver"
	"fmt"
)

type HelloWorldStore struct {
	Names []string
	Next  httpserver.Handler
}

func (hws HelloWorldStore) ServeHTTP(w http.ResponseWriter, r *http.Request) (int, error) {
	if httpserver.Path(r.URL.Path).Matches("/hello") {
		fmt.Fprintln(w, "Hello,")
		for _, name := range(hws.Names) {
			fmt.Fprintln(w, name)
		}
		return 0, nil // signal to caddy we have written to the request
	}

	return hws.Next.ServeHTTP(w, r)
}
